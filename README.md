# Notes from _The Programmer's Brain_

Notes from my reading of [_The Programmer's Brain_](https://www.manning.com/books/the-programmers-brain) by Felienne Hermans.

The main idea is to understand how your brain works, so you can understand its strengths and weaknesses, using its strengths and compensating for its weaknesses.



## cognitive processes

-   _long-term memory_ or LTM: where all memories are permanently stored
    -   without revisiting information, information has a half-life of about 10 hours — spaced repetition helps it be maintained
    -   for programming, the LTM stores information about programming language, the code base at hand, the domain, how you've solved previous problems in the past, etc.
    -   implicit/procedural memory — what you can "do without thinking"
    -   explicit/declarative memory — facts you remember
        -   includes episodic memory of experiences — automatically created, but can be strengthened
        -   semantic memory, such as meaning, concepts, or facts — trained with flashcards
-   _short-term memory_ or STM: temporary storage as you gather information
    -   only lasts for 30 seconds; information is lost if not put in LTM
    -   limited to 2 to 6 "things" at a time — but chunking can help
-   _working memory_: where "thinking" happens
    -   access LTM and STM
    -   logic of tracing through code
    -   also only 2 to 6 things at a time — "cognitive load" is a measure of capacity
    -   (some folks combine ideas of STM and working memory)
    -   chunking can help here, too

## chunking

You can seem to hold more in your STM if you can connect ideas in your STM with ideas in your LTM. For instance, "that's a standard payroll deduction calculation" rather than "you sum up the compensations, then remove the before-tax deductions corresponding to their W2 Box 12 codes." If the latter is in your LTM, then you can "chunk" it into the shorter summary.

The more information you have stored about a specific topic, the easier it is to create appropriate chunks.

## cognitive load

The capacity of the working memory to process information. Like STM, 2 to 6 items at a time, but chunking can help. Three main types:

-   *intrinsic load*: how complex the problem is itself
-   *extraneous load*: what outside distractions add to the problem
    -   different things for different folks — depending on knowledge of programming language, domain, etc.
-   *germane load*: the effort required to store information to the LTM
    -   when you are maxed in intrinsic and extraneous load, **you do not have enough space to store what you did to LTM!!**

See below for tips on how to augment your cognitive load — you maybe cannot increase your cognitive capacity, but you *can* lean on other tools to help!



## reading code

-   60% of programmer's time is reading code
-   looking for specific, relevant information — how can you find it most quickly?
-   tip: temporarily refactor code to be more comfortable for your understanding
    -   inline function calls
    -   reorder methods
    -   rename variables
    -   use syntax more familiar to you
-   tip: offload using memory aids
    -   create a dependency graph — scribble on the code and connect where variables, functions, classes, etc. are used
    -   use a state table to track changes of variable values
-   tip: mark variables with icons representing their function: fixed value, stepper, flag, walker, most recent holder, most wanted holder, gatherer, container, follower, organizer, temporary
-   tip: start at a focal point, and start expanding your knowledge about the overall plan from there
-   reading code is more similar to reading text than to doing math
-   tip: reflect on which lines are important (varies between people, but still useful)
-   when code is more complex or you have less knowledge about it or the domain, creating an accurate mental model is more work
    -   inaccurate mental models can really lead you down a strange path

    

## writing code to be understandable

-   writing code that lets you "chunk" it is easier to read
    -   what is chunk-able is different for different people, based on their knowledge and experience!
    -   using common patterns in a code base helps chunking
-   comments
    -   beginners focus a lot more on comments than experts do
    -   high-level comments summarizing blocks of code can help chunk (i.e. "[these 30 lines] are for transferring donations from one individual to another")
-   beacons: things in the code that help indicate what's going on
    -   variable and function names (i.e. a variable called "individual" tells us that we aren't working at the moment with an email)
    -   high-level comments
-   naming
    -   consistent naming across a code base — supports chunking
    -   better to thinking about good naming at a _different time_ than when writing code, due to cognitive overload
    -   words are usually better than abbreviations or single letters

## remembering

We can work faster the less we are at a loss for information, including information about syntax, semantics, language behavior, etc.

Looking things up has several costs:

-   we have to spend mental energy on the look up process to read/comprehend
-   likely will have to remove things from the STM for the look up process
-   distractions (especially if web browser)

### how can you remember more?

-   spaced repetition — memorization work spaced out over time
    -   this helps deal with the half-life of memories in the LTM
    -   the longer the space, the better — up to maybe one or two months? — in one study the optimal repetition was 26 study sessions, 8 weeks apart
-   flash cards — easy method
-   the _act_ of trying to remember _helps_ us remember more(!) (i.e. don't immediately give up if you can't remember something — try for a moment!)
-   actively thinking about the information — relating it to existing memories

### automatization

Once you have practiced a skill so many times you don't need to think about it. (This is the LTM's *implicit memory*.) Doing this with smaller problems allows you to solver larger and more complex problems.

Things you have automatized *do not add to your cognitive load.*

**Deliberate practice** is the way to store the activity (i.e. `map`ping from one array format to another or making a server request) in your implicit memory. For instance, you can write a bunch of similar-but-different programs. Again, *spaced repetition* is key.

## bugs in thinking

-   transferring details about one programming language or domain to another — helpful when correct, but **harmful** when wrong, especially when you odn't know
-   fixing knowledge already stored in the LTM is much harder than learning afresh

## cognitive augmentation

-   when searching through a code base, your intrinsic and extraneous loads are maxed out, so you don't have space to save to your LTM. therefore, **write notes** to help yourself
    -   you can leave comments in the code about why you visited there
    -   what are you looking for? what have you found? where will you search next?
    -   comments can be in the code or on paper
    -   sometimes, you need to map out how function calls relate to each other
-   when reading code, your working memory is maxed out (2-6 items) and likely needs help
    -   write down a model of what's going on
    -   refactor unclear code into more familiar constructs (i.e. a `for` loop instead of `.map` or whatever)
-   taking notes helps deal with interruptions, too
-   **documenting decisions** can help store your mental model